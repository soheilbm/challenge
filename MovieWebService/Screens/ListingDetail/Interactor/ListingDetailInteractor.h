//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingDetailProtocols.h"
#import "Film.h"

@interface ListingDetailInteractor : NSObject <ListingDetailInteractorInputProtocol>
@property (nonatomic, weak) id <ListingDetailInteractorOutputProtocol> presenter;
@property (nonatomic, strong) Film *film;
- (instancetype)initWithModel:(Film *)film NS_DESIGNATED_INITIALIZER;
@end
