//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingDetailPresenter.h"
#import "ListingDetailWireframe.h"

@implementation ListingDetailPresenter

-(void)fetchData {
    [_view updateViewWith:_interactor.film];
}

@end
