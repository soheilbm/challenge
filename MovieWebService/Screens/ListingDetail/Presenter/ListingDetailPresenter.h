//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingDetailProtocols.h"

@class ListingDetailWireFrame;

@interface ListingDetailPresenter : NSObject <ListingDetailPresenterProtocol, ListingDetailInteractorOutputProtocol>

@property (nonatomic, weak) id <ListingDetailViewProtocol> view;
@property (nonatomic, strong) id <ListingDetailInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ListingDetailWireFrameProtocol> wireFrame;

@end
