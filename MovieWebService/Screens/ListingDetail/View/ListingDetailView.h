//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListingDetailProtocols.h"

@interface ListingDetailView : UIViewController <ListingDetailViewProtocol>

@property (nonatomic, strong) id <ListingDetailPresenterProtocol> presenter;
@property (strong, nonatomic) IBOutlet UILabel *directorName;
@property (strong, nonatomic) IBOutlet UILabel *actorName;
@property (strong, nonatomic) IBOutlet UILabel *actorScreenName;
@property (strong, nonatomic) IBOutlet UILabel *direcotrNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *actorNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *actorScreenNameTitle;


@end
