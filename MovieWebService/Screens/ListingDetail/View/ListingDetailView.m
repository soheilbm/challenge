//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingDetailView.h"
#import "TappableLabel.h"

@interface ListingDetailView()<TappableLabelDelegate>
@property (strong, nonatomic) TappableLabel *tapToShowMore;
@end

@implementation ListingDetailView

- (void)loadNibs {
    UIView *aView = [[[NSBundle mainBundle] loadNibNamed:@"DetailViewController" owner:self options:nil] objectAtIndex:0];
    aView.frame = self.view.frame;
    
    [self.view addSubview:aView];
}

- (void)addTouchLabel {
    self.tapToShowMore = [[TappableLabel alloc] initWithFrame:CGRectMake(20.0f, 78.0f, 280.0f, 44.0f)];
    self.tapToShowMore.font = [UIFont systemFontOfSize:14.0f];
    self.tapToShowMore.delegate = self;
    self.tapToShowMore.text = @"Tap here to show more";
    [self.view addSubview:self.tapToShowMore];
}

-(void) updateViewWith:(Film *)film {
    self.direcotrNameTitle.text = film.director.name;
    Actor *actor = (Actor *)[film.cast objectAtIndex:0];
    if (actor) {
        self.actorNameTitle.text = actor.name;
        self.actorScreenNameTitle.text = actor.screenName;
    }
}

#pragma mark - Delegate

- (void)didReceiveTouch {
    self.tapToShowMore.hidden = YES;
    self.actorName.hidden = NO;
    self.actorScreenName.hidden = NO;
    self.actorNameTitle.hidden = NO;
    self.actorScreenNameTitle.hidden = NO;
}

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNibs];
    [self addTouchLabel];
    [_presenter fetchData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

@end
