//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingDetailProtocols.h"
#import "ListingDetailView.h"
#import "ListingDetailInteractor.h"
#import "ListingDetailPresenter.h"
#import "ListingDetailWireframe.h"
#import <UIKit/UIKit.h>
#import "Film.h"
#import "Router.h"

@interface ListingDetailWireFrame : NSObject <WireFrameProtocol>
- (instancetype)initWithModel:(Film *)film NS_DESIGNATED_INITIALIZER;
@end
