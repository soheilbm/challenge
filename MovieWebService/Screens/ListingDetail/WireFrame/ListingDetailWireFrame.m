//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingDetailWireFrame.h"

@interface ListingDetailWireFrame()
@property (nonatomic,strong) Film *film;
@end

@implementation ListingDetailWireFrame

- (instancetype)initWithModel:(Film *)film {
    self = [super init];
    if (self) {
        _film = film;
    }
    return self;
}

- (UIViewController*)getModuleViewController {
    // Generating module components
    id <ListingDetailViewProtocol> view = [[ListingDetailView alloc] init];
    id <ListingDetailPresenterProtocol, ListingDetailInteractorOutputProtocol> presenter = [ListingDetailPresenter new];
    id <ListingDetailInteractorInputProtocol> interactor = [[ListingDetailInteractor alloc] initWithModel:_film];
    id <ListingDetailWireFrameProtocol> wireFrame= [ListingDetailWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
        
    return view;
}

@end
