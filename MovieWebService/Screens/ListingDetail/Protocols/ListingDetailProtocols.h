//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Film.h"

@protocol ListingDetailInteractorOutputProtocol;
@protocol ListingDetailInteractorInputProtocol;
@protocol ListingDetailViewProtocol;
@protocol ListingDetailPresenterProtocol;


@class ListingDetailWireFrame;

@protocol ListingDetailViewProtocol
@required
@property (nonatomic, strong) id <ListingDetailPresenterProtocol> presenter;
-(void) updateViewWith:(Film *)film;
@end

@protocol ListingDetailWireFrameProtocol
@required
+ (void)presentListingDetailModuleFrom:(id)fromView;
@end

@protocol ListingDetailPresenterProtocol
@required
@property (nonatomic, weak) id <ListingDetailViewProtocol> view;
@property (nonatomic, strong) id <ListingDetailInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <ListingDetailWireFrameProtocol> wireFrame;
-(void)fetchData;
@end

@protocol ListingDetailInteractorOutputProtocol
@end

@protocol ListingDetailInteractorInputProtocol
@required
@property (nonatomic, weak) id <ListingDetailInteractorOutputProtocol> presenter;
@property (nonatomic, strong) Film *film;
@end




