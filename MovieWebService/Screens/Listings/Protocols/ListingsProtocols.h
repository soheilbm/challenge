//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Router.h"
#import "Film.h"
#import <UIKit/UIKit.h>

@protocol ListingsInteractorOutputProtocol;
@protocol ListingsInteractorInputProtocol;
@protocol ListingsViewProtocol;
@protocol ListingsPresenterProtocol;
@protocol ListingsLocalDataManagerInputProtocol;

@class ListingsWireFrame;

@protocol ListingsViewProtocol
@required
@property (nonatomic, strong) id <ListingsPresenterProtocol> presenter;
- (void) reloadTable;
- (void) pushViewController:(UIViewController *)vc;
@end


@protocol ListingsPresenterProtocol
@required
@property (nonatomic, weak) id <ListingsViewProtocol> view;
@property (nonatomic, strong) id <ListingsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <WireFrameProtocol> wireFrame;
-(void)fetchData;
@end

@protocol ListingsInteractorOutputProtocol
-(void) updateView;
@end

@protocol ListingsInteractorInputProtocol
@required
@property (nonatomic, weak) id <ListingsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <ListingsLocalDataManagerInputProtocol> localDataManager;
@property (nonatomic, strong) NSArray<Film *> *films;
-(void) fetchData;
@end


@protocol ListingsDataManagerInputProtocol
@end


@protocol ListingsLocalDataManagerInputProtocol <ListingsDataManagerInputProtocol>
- (void)fetchData:(void (^)(NSArray<Film *> *))completionBlock;
@end


