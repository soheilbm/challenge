//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListingsProtocols.h"

@interface ListingsView : UIViewController <ListingsViewProtocol>

@property (nonatomic, strong) id <ListingsPresenterProtocol> presenter;

@end
