//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingsView.h"
#import "MovieCell.h"

@interface ListingsView()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ListingsView

- (void)loadNibs {
    UIView *aView = [[[NSBundle mainBundle] loadNibNamed:@"ListViewController" owner:self options:nil] objectAtIndex:0];
    aView.frame = self.view.frame;
    self.tableView.delegate = self.presenter;
    self.tableView.dataSource = self.presenter;
    
    [self.view addSubview:aView];
}

- (void) reloadTable {
    [self.tableView reloadData];
}

- (void) pushViewController:(UIViewController *)vc {
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =  @"Root View Controller";
    
    [self loadNibs];
    [self.presenter fetchData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

@end
