//
//  MovieCell.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "MovieCell.h"
#import "Film.h"
#import "DateHelper.h"

@implementation MovieCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setupCell:(Film *)film {
    self.name.text = film.name;
    
    self.date.text = [[DateHelper sharedInstance] getStringDate: film.releaseDate ];
    self.filmRating.text = [film getFilmRatingString];
    self.rating.text = [[NSNumber numberWithDouble:film.rating] stringValue];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
