//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingsProtocols.h"
#import "ListingsView.h"
#import "ListingsLocalDataManager.h"
#import "ListingsInteractor.h"
#import "ListingsPresenter.h"
#import "ListingsWireframe.h"
#import <UIKit/UIKit.h>


@interface ListingsWireFrame : NSObject <WireFrameProtocol>

@end
