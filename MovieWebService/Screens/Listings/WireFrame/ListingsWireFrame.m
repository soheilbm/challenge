//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingsWireFrame.h"

@implementation ListingsWireFrame

- (UIViewController*)getModuleViewController {
    // Generating module components
    id <ListingsViewProtocol> view = [[ListingsView alloc] init];
    id <ListingsPresenterProtocol, ListingsInteractorOutputProtocol> presenter = [ListingsPresenter new];
    id <ListingsInteractorInputProtocol> interactor = [ListingsInteractor new];
    id <ListingsLocalDataManagerInputProtocol> localDataManager = [ListingsLocalDataManager new];
    id <WireFrameProtocol> wireFrame= [ListingsWireFrame new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.localDataManager = localDataManager;
    
    return view;
}

@end
