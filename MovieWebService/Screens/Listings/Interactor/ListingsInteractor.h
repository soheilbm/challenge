//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingsProtocols.h"
#import "Film.h"

@interface ListingsInteractor : NSObject <ListingsInteractorInputProtocol>

@property (nonatomic, strong) NSArray<Film *> *films;
@property (nonatomic, weak) id <ListingsInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <ListingsLocalDataManagerInputProtocol> localDataManager;

@end
