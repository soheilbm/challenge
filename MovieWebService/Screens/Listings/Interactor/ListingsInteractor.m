//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingsInteractor.h"

@implementation ListingsInteractor

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.films = [[NSArray alloc] init];
    }
    return self;
}

-(void) fetchData {
    __weak ListingsInteractor *weakSelf = self;
    [self.localDataManager fetchData:^(NSArray<Film *> *films) {
        weakSelf.films = films;
        [weakSelf.presenter updateView];
    }];
}

@end
