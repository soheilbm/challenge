//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListingsPresenter.h"
#import "ListingsWireframe.h"
#import "MovieCell.h"
#import "ListingDetailWireFrame.h"

@interface ListingsPresenter() <UITableViewDataSource, UITableViewDelegate>
@end

static NSString *CellIdentifier = @"MovieCell";

@implementation ListingsPresenter

-(void)fetchData{
    [self.interactor fetchData];
}

-(void)updateView {
    [self.view reloadTable];
}

#pragma mark - UITableView Delegate & DataSource Lifecycle

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.interactor.films.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MovieCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MovieCell" owner:self options:nil] objectAtIndex:0];
    }
    
    [cell setupCell:[self.interactor.films objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Film *film = [self.interactor.films objectAtIndex:indexPath.row];
    ListingDetailWireFrame *listingDetailVc = [[ListingDetailWireFrame alloc] initWithModel:film];
    [self.view pushViewController:[listingDetailVc getModuleViewController]];
}

@end
