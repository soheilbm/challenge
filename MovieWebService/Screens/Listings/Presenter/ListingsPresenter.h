//
// Created by Soheil on 11/4/17
// Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingsProtocols.h"

@class ListingsWireFrame;

@interface ListingsPresenter : NSObject <ListingsPresenterProtocol, ListingsInteractorOutputProtocol>

@property (nonatomic, weak) id <ListingsViewProtocol> view;
@property (nonatomic, strong) id <ListingsInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <WireFrameProtocol> wireFrame;

@end
