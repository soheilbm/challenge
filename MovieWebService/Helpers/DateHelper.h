//
//  DateHelper.h
//  MovieWebService
//
//  Created by Soheil on 11/4/17.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject
+(DateHelper *) sharedInstance;
-(NSString *)getStringDate:(NSDate *)date;
@end
