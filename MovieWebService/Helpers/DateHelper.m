//
//  DateHelper.m
//  MovieWebService
//
//  Created by Soheil on 11/4/17.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "DateHelper.h"

@interface DateHelper()
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@end

@implementation DateHelper
+(DateHelper *) sharedInstance {
    static DateHelper * sharedObject;
    
    @synchronized(self)
    {
        if (!sharedObject)
            sharedObject = [[DateHelper alloc] init];
        sharedObject.dateFormatter = [[NSDateFormatter alloc] init];
        [sharedObject.dateFormatter setDateFormat:@"dd/MM/yyyy"];
        return sharedObject;
    }
}

-(NSString *)getStringDate:(NSDate *)date {
    return [_dateFormatter stringFromDate:date];
}

@end
