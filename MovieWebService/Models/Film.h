//
//  Film.h
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Actor.h"
#import "Director.h"

typedef NS_ENUM(NSUInteger, FilmRating) {
    G = 0,
    PG,
    PG13,
    R,
    NC17
};

NS_ASSUME_NONNULL_BEGIN

@interface Film : NSObject
@property (nonatomic) FilmRating filmRating;
@property (strong, nonatomic) NSArray *languages;
@property (strong, nonatomic) NSDate *releaseDate;
@property (strong, nonatomic) NSArray<Actor*> *cast;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) double rating;
@property (nonatomic, strong) Director *director;
@property (nonatomic) BOOL nominated;

- (id)initWithData:(NSDictionary *)data;
- (NSString *) getFilmRatingString;
@end

NS_ASSUME_NONNULL_END
