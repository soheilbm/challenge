//
//  Film.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "Film.h"
#import "Actor.h"
#import "Director.h"
#import "DateHelper.h"

@implementation Film

- (id)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
        self.filmRating = [[data objectForKey:@"filmRating"] integerValue];
        self.languages = [data objectForKey:@"languages"];
        self.releaseDate = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"releaseDate"] doubleValue]];
        self.name = [data objectForKey:@"name"];
        self.rating = [[data objectForKey:@"rating"] doubleValue];
        self.nominated = [[data objectForKey:@"nominated"] boolValue];
        
        Director *director = [[Director alloc] initWithData:[data objectForKey:@"director"]];
        director.film = self;
        self.director = director;
        
        NSMutableArray *castsList = [[NSMutableArray alloc] init];
        NSArray *castsData = [data objectForKey:@"casts"];
        for (NSDictionary *castData in castsData) {
            Actor *actor = [[Actor alloc] initWithData:castData];
            actor.film = self;
            [castsList addObject:actor];
        }
        
        self.cast = castsList;
    }
    return self;
}

- (NSString *) getFilmRatingString {
    NSString *filmRatingText;
    
    switch (self.filmRating) {
        case G:
            filmRatingText = @"G";
        case PG:
            filmRatingText = @"PG";
        case PG13:
            filmRatingText = @"PG13";
        case R:
            filmRatingText = @"R";
        default:
            break;
    }
    
    return filmRatingText;
}

@end
