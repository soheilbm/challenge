//
//  Router.h
//  MovieWebService
//
//  Created by Soheil on 11/4/17.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol WireFrameProtocol
@required
- (UIViewController*)getModuleViewController;
@end


@interface Router : NSObject
- (UIViewController *) getMainViewController;

@end
