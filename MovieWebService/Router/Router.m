//
//  Router.m
//  MovieWebService
//
//  Created by Soheil on 11/4/17.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "Router.h"
#import "ListingsWireFrame.h"

@interface Router()
@property (weak, nonatomic) IBOutlet UINavigationController *navigationController;
@end

@implementation Router

- (UIViewController *) getMainViewController {
    ListingsWireFrame *listingVC = [[ListingsWireFrame alloc]init];
    UIViewController *mainViewController = (UIViewController *)[listingVC getModuleViewController];
    [mainViewController.view setFrame:[UIScreen mainScreen].bounds];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
    self.navigationController.navigationBar.translucent = NO;
    return self.navigationController;
}

@end
