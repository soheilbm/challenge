//
//  MovieWebServiceTests.m
//  MovieWebServiceTests
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DateHelper.h"

@interface MovieWebServiceTests : XCTestCase {
    DateHelper *sut;
}
@end

@implementation MovieWebServiceTests

- (void)setUp
{
    [super setUp];
    sut = [DateHelper sharedInstance];
}

- (void)tearDown
{
    [super tearDown];
    sut = nil;
}

- (void)testStringDate {
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *expectedDate = [dateFormatter stringFromDate:date];
    NSString *actual = [sut getStringDate:date];
    
    XCTAssertTrue([expectedDate isEqualToString:actual]);
}
@end
