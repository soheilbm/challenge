//
//  ModelTests.m
//  MovieWebService
//
//  Created by Soheil on 11/4/17.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Film.h"
#import "DateHelper.h"


@interface ModelTests : XCTestCase{
    Film *sut;
    NSDictionary *data;
}
@end

@implementation ModelTests

- (void)setUp {
    [super setUp];
    
    data = @{
             @"filmRating" : @3,
             @"languages": @[
                     @"English",
                     @"Thai"
                     ],
             @"nominated": @1,
             @"releaseDate": @1350000000,
             @"casts": @[
                     @{
                         @"dateOfBirth": @-436147200,
                         @"nominated": @1,
                         @"name": @"Bryan Cranston",
                         @"screenName": @"Jack Donnell",
                         @"biography": @"Bryan Lee Cranston is an American actor, voice actor, writer and director."
                         }
                     ],
             @"name": @"Argo",
             @"rating": @7.8,
             @"director": @{
                     @"dateOfBirth": @82684800,
                     @"nominated": @1,
                     @"name": @"Ben Affleck",
                     @"biography": @"Benjamin Geza Affleck was born on August 15, 1972 in Berkeley, California, USA but raised in Cambridge, Massachusetts, USA."
                     }
             };
    
    sut = [[Film alloc] initWithData:data];
}

- (void)tearDown {
    [super tearDown];
    sut = nil;
    data = nil;
}

- (void)testFilm {
    XCTAssertTrue([sut.name isEqualToString:@"Argo"]);
    XCTAssertEqual(sut.filmRating, R);
    XCTAssertEqual(sut.releaseDate, [NSDate dateWithTimeIntervalSince1970:(double)1350000000]);
    XCTAssertTrue([[[DateHelper sharedInstance] getStringDate:sut.releaseDate] isEqualToString:@"12/10/2012"]);
    XCTAssertEqual(sut.rating, (double)7.8);
    XCTAssertTrue(sut.nominated);
    
}

- (void) testDirector {
    XCTAssertTrue([sut.director.name isEqualToString:@"Ben Affleck"]);
    XCTAssertTrue([sut.director.biography isEqualToString:@"Benjamin Geza Affleck was born on August 15, 1972 in Berkeley, California, USA but raised in Cambridge, Massachusetts, USA."]);
    XCTAssertEqual(sut.director.dateOfBirth, [NSDate dateWithTimeIntervalSince1970:(double)82684800]);
}


- (void) testActor{
    XCTAssertEqual(sut.cast.count, 1);
    XCTAssertEqual(sut.cast[0].dateOfBirth, [NSDate dateWithTimeIntervalSince1970:(double)-436147200]);
    XCTAssertTrue([sut.cast[0].name isEqualToString:@"Bryan Cranston"]);
    XCTAssertTrue([sut.cast[0].screenName isEqualToString:@"Jack Donnell"]);
    XCTAssertTrue([sut.cast[0].biography isEqualToString:@"Bryan Lee Cranston is an American actor, voice actor, writer and director."]);
}

@end
